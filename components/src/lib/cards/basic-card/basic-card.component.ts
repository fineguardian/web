import { Component } from '@angular/core';

@Component({
  selector: 'lib-basic-card',
  templateUrl: './basic-card.component.html',
  styleUrls: ['./basic-card.component.scss'],
})
export class BasicCardComponent {}
