import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicCardComponent } from './cards/basic-card/basic-card.component';
import { BasicButtonComponent } from './buttons/basic-button/basic-button.component';

@NgModule({
  imports: [CommonModule],
  declarations: [BasicCardComponent, BasicButtonComponent],
})
export class ComponentsModule {}
